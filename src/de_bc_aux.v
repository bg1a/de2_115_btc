module de_echo_str(
   input wire clk,
   input wire [2:0] idx,
   output wire [7:0] ch
);

   reg [7:0] ch_reg;

   always@(posedge clk)
      case(idx)
         3'd0: ch_reg <= 8'h0A;
         3'd1: ch_reg <= 8'h0D;
         3'd2: ch_reg <= "h";
         3'd3: ch_reg <= "a";
         3'd4: ch_reg <= "s";
         3'd5: ch_reg <= "h";
         3'd6: ch_reg <= ":";
         3'd7: ch_reg <= " ";
      endcase
   assign ch = ch_reg;
endmodule

module de_hex2ch(
   input wire [7:0] hex,
   output wire [7:0] ch_high,
   output wire [7:0] ch_low
);
   wire [3:0] msb, lsb;

   assign lsb = hex[0 +: 4];
   assign msb = hex[4 +: 4];
   assign ch_high = (msb < 4'hA) ? (msb + 8'h30) : (msb + 8'h57);
   assign ch_low = (lsb < 4'hA) ? (lsb + 8'h30) : (lsb + 8'h57);
endmodule

module de_serial(
   input wire clk, reset,
   input wire [255:0] tx_hash,
   input wire [31:0] tx_nonce,
   input wire tx_start,

   output wire [(SERIAL_MSG_LEN*8 - 1):0] rx_data,
   output wire rx_data_rdy,
   
   input wire rx_line,
   output wire tx_line
);
   parameter CLK = 5000_000;
   parameter BAUD = 38400;
   parameter SERIAL_MSG_LEN = 128;
   parameter UART_MSG_LEN = 80;

   localparam  SERIAL_IDLE       = 2'b00,
               SERIAL_RX         = 2'b01,
               SERIAL_MSG_DONE   = 2'b10,
               SERIAL_DATA_TX    = 2'b11;

   reg [1:0] state, state_next;

   localparam TX_DATA_SZ = 76;
   wire [511:0] tx_hash_ascii;
   wire [63:0] tx_nonce_ascii;
   
   generate
      genvar i;
      for(i = 0; i < 32; i = i + 1) begin: gen_ascii
         wire [7:0] in_hex;
         wire [7:0] out_ch_l, out_ch_h;
         
         assign in_hex = tx_hash[i*8 +: 8];
         assign tx_hash_ascii[i*16 +: 8] = out_ch_l;
         assign tx_hash_ascii[(i*16 + 8) +: 8] = out_ch_h;
         de_hex2ch h2ch(in_hex, out_ch_h, out_ch_l);
      end
   endgenerate
   
   generate
      genvar k;
      for(k = 0; k < 4; k = k + 1) begin: gen_nonce
         wire [7:0] in_hex;
         wire [7:0] out_ch_l, out_ch_h;
   
         assign in_hex = tx_nonce[k*8 +: 8];
         assign tx_nonce_ascii[k*16 +: 8] = out_ch_l;
         assign tx_nonce_ascii[(k*16 + 8) +: 8] = out_ch_h;
         de_hex2ch h2ch(in_hex, out_ch_h, out_ch_l);
      end
   endgenerate
   
   wire [7:0] uart_rx_net, uart_tx_net;
   wire uart_rx_rdy, uart_tx_start, uart_tx_done;
   reg uart_tx_active;

   de_uart #(.CLK_HZ(CLK), .BAUD(BAUD)) uart(
      .clk(clk), .reset(reset),
      .rx_line(rx_line),
      .rx_byte(uart_rx_net),
      .rx_rdy(uart_rx_rdy),
      .tx_line(tx_line),
      .tx_byte(uart_tx_net),
      .tx_done(uart_tx_done),
      .tx_en(uart_tx_start)
   );

   /***************************************************************************************************
   * Serial FSM
   ***************************************************************************************************/
   wire [2:0] serial_echo_idx;
   wire [7:0] serial_echo_ch;
   de_echo_str echo_str(
      .clk(clk),
      .idx(serial_echo_idx),
      .ch(serial_echo_ch)
   );

   reg [7:0] serial_rx_cnt, serial_rx_cnt_next;
   reg [7:0] serial_rx_byte, serial_rx_byte_next;
   reg serial_rx_rdy, serial_rx_rdy_next;
   reg [7:0] serial_rx_msg_idx, serial_rx_msg_idx_next; 

   reg [7:0] serial_tx_byte, serial_tx_byte_next;
   reg [7:0] serial_tx_echo_cnt, serial_tx_echo_cnt_next;
   reg [8:0] serial_tx_data_cnt, serial_tx_data_cnt_next;
   reg [(TX_DATA_SZ*8 - 1):0] serial_tx_data, serial_tx_data_next;
   reg serial_tx_rdy, serial_tx_rdy_next;
   wire serial_tx_start;

   always@(posedge clk, posedge reset)
      if(reset) begin
         state <= SERIAL_RX;
         serial_rx_cnt <= 8'b0;
         serial_rx_byte <= 8'b0;
         serial_rx_rdy <= 1'b0;
         serial_rx_msg_idx <= 8'b0;
         
         serial_tx_byte <= 8'b0;
         serial_tx_rdy <= 1'b0;
         serial_tx_echo_cnt <= 8'b0;
         serial_tx_data_cnt <= 9'b0;
         serial_tx_data <= 0;
      end
      else begin
         state <= state_next;
         serial_rx_cnt <= serial_rx_cnt_next;
         serial_rx_byte <= serial_rx_byte_next;
         serial_rx_rdy <= serial_rx_rdy_next;
         serial_rx_msg_idx <= serial_rx_msg_idx_next;
         
         serial_tx_byte <= serial_tx_byte_next;
         serial_tx_rdy <= serial_tx_rdy_next;
         serial_tx_echo_cnt <= serial_tx_echo_cnt_next;
         serial_tx_data_cnt <= serial_tx_data_cnt_next;
         serial_tx_data <= serial_tx_data_next;
      end
         
   always@(*) begin
      state_next = state;
      serial_rx_cnt_next = serial_rx_cnt;
      serial_rx_byte_next = serial_rx_byte;
      serial_rx_rdy_next = serial_rx_rdy;
      serial_rx_msg_idx_next = serial_rx_msg_idx;
      
      serial_tx_byte_next = serial_tx_byte;
      serial_tx_rdy_next = serial_tx_rdy;
      serial_tx_echo_cnt_next = serial_tx_echo_cnt;
      serial_tx_data_cnt_next = serial_tx_data_cnt;
      serial_tx_data_next = serial_tx_data;
      
      case (state)
         SERIAL_IDLE: begin
            serial_rx_cnt_next = 8'b0;
            serial_rx_msg_idx_next = 8'b0;
            serial_rx_byte_next = 8'b0;
            serial_rx_rdy_next = 1'b0;
            
            serial_tx_echo_cnt_next = 8'b0;
            serial_tx_byte_next = serial_echo_ch;
            serial_tx_data_cnt_next = 8'b0;
            serial_tx_rdy_next = 1'b0;
            serial_tx_data_next = 0;
            
            if(uart_rx_rdy)
               state_next = SERIAL_RX;
         end
         SERIAL_RX: begin
            serial_rx_rdy_next = uart_rx_rdy_reg;
            serial_tx_rdy_next = uart_rx_rdy_reg;

            if(uart_rx_rdy_reg) begin
               serial_rx_byte_next = uart_rx_byte;
               serial_tx_byte_next = uart_rx_byte;
            end

            if(uart_rx_cnt >= UART_MSG_LEN) begin
               state_next = SERIAL_MSG_DONE;
               serial_rx_cnt_next = uart_rx_cnt;
               serial_rx_msg_idx_next = uart_rx_cnt;
            end
         end
         SERIAL_MSG_DONE: begin
            if(~uart_tx_active & ~serial_tx_rdy) begin
               serial_tx_rdy_next = 1'b1;
               serial_tx_echo_cnt_next = serial_tx_echo_cnt + 1'b1;
               serial_tx_byte_next = serial_echo_ch;
            end
            else begin
               serial_tx_rdy_next = 1'b0;
            end
            
            if(serial_rx_msg_idx < SERIAL_MSG_LEN) begin
               serial_rx_rdy_next = 1'b1;
               serial_rx_msg_idx_next = serial_rx_msg_idx + 1'b1;
               if(serial_rx_cnt == serial_rx_msg_idx)
                  serial_rx_byte_next = 8'b1000_0000;
               
               else if((SERIAL_MSG_LEN - 1) == serial_rx_msg_idx)
                  serial_rx_byte_next = {serial_rx_cnt[4:0], 3'b0};
               else if((SERIAL_MSG_LEN - 2) == serial_rx_msg_idx)
                  serial_rx_byte_next = {3'b0, serial_rx_cnt[7:5]};
               else
                  serial_rx_byte_next = 8'b0;
            end
            else
               serial_rx_rdy_next = 1'b0;
            
            if((serial_rx_msg_idx >= SERIAL_MSG_LEN) && (serial_tx_echo_cnt > 3'd7) && (serial_tx_start)) begin
               state_next = SERIAL_DATA_TX;
               serial_tx_data_next = tx_data_reg;
            end
         end
         SERIAL_DATA_TX: begin
            if(~uart_tx_active & ~serial_tx_rdy) begin
               serial_tx_rdy_next = 1'b1;
               serial_tx_data_cnt_next = serial_tx_data_cnt + 1'b1;
               serial_tx_byte_next = serial_tx_data[(TX_DATA_SZ - 1)*8 +: 8];
               serial_tx_data_next = {serial_tx_data[0 +: (TX_DATA_SZ - 1)*8], 8'b0};
            end
            else begin
               serial_tx_rdy_next = 1'b0;
            end
            
            serial_rx_rdy_next = 1'b0;
            
            if(serial_tx_data_cnt >= TX_DATA_SZ)
               state_next = SERIAL_IDLE;
         end
      default:
         state_next = SERIAL_IDLE;
         
      endcase   
   end

   assign serial_echo_idx = serial_tx_echo_cnt[2:0];
   /***************************************************************************************************
   * UART, RX 
   ***************************************************************************************************/
   reg [7:0] uart_rx_byte;
   reg uart_rx_rdy_reg;
   reg [7:0] uart_rx_cnt;

   always@(posedge clk, posedge reset)
      if(reset) begin
         uart_rx_byte <= 8'b0;
         uart_rx_rdy_reg <= 1'b0;
         uart_rx_cnt <= 8'b0;
      end   
      else begin
         uart_rx_byte <= (state == SERIAL_IDLE) ? 8'b0 : 
                         (uart_rx_rdy) ? uart_rx_net : uart_rx_byte;
         uart_rx_rdy_reg <= (state == SERIAL_IDLE) ? 1'b0 :
                            uart_rx_rdy;
         uart_rx_cnt <= (state == SERIAL_IDLE) ? 8'b0 :
                        (uart_rx_rdy) ? (uart_rx_cnt + 1'b1) : uart_rx_cnt;
      end
      
   /***************************************************************************************************
   * UART, TX
   ***************************************************************************************************/   
   always@(posedge clk, posedge reset)
      if(reset)
         uart_tx_active <= 1'b0;
      else
         uart_tx_active <= (uart_tx_active) ? (~uart_tx_done) : (serial_tx_rdy);
         
   assign uart_tx_start = serial_tx_rdy;
   assign uart_tx_net = serial_tx_byte;

   /***************************************************************************************************
   * Output data 
   ***************************************************************************************************/
   reg [(SERIAL_MSG_LEN*8 - 1):0] rx_data_reg;
   reg [7:0] rx_data_cnt;
   always@(posedge clk, posedge reset)
      if(reset) begin
         rx_data_reg <= 0;
         rx_data_cnt <= 8'b0;
      end
      else begin
         rx_data_reg <= (SERIAL_IDLE == state) ? 0 :
                        (serial_rx_rdy) ? {rx_data_reg[0 +: (SERIAL_MSG_LEN - 1)*8], serial_rx_byte} :
                        rx_data_reg;
         rx_data_cnt <= (SERIAL_IDLE == state) ? 8'b0 :
                        (serial_rx_rdy) ? (rx_data_cnt + 1'b1) : rx_data_cnt;
      end

   assign rx_data = rx_data_reg;
   assign rx_data_rdy = (rx_data_cnt == SERIAL_MSG_LEN);   

   /***************************************************************************************************
   * Input data 
   ***************************************************************************************************/
   reg [(TX_DATA_SZ*8 - 1):0] tx_data_reg;
   reg tx_start_reg;

   always@(posedge clk, posedge reset)
      if(reset) begin
         tx_start_reg <= 1'b0;
         tx_data_reg <= 0;
      end
      else begin
         tx_start_reg <= (state == SERIAL_IDLE) ? 1'b0 :
                         (tx_start) ? 1'b1 : tx_start_reg;
         tx_data_reg <=  (state == SERIAL_IDLE) ? 1'b0 :
                         (tx_start) ? {tx_hash_ascii, 8'h0A, 8'h0D, tx_nonce_ascii, 8'h0A, 8'h0D} :
                                       tx_data_reg;
      end

   assign serial_tx_start = tx_start_reg;
      
endmodule
