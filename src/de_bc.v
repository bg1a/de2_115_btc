module de_bc(
   input wire clk,
   input wire reset,

	input wire uart_rx,
   output wire uart_tx
);
   parameter CLK = 5000_000; // 5MHz default clock
   parameter BAUD_RATE = 38400;

   reg [1:0] state, state_next;
   localparam  DE_BTC_IDLE = 2'b00,
               DE_BTC_INIT = 2'b01,
               DE_BTC_BUSY = 2'b10,
               DE_BTC_SEND = 2'b11;

   wire [1023:0] rx_net;
	reg [1023:0] rx_reg;
   reg [31:0] nonce_reg;
   wire rx_ready;
   reg [255:0] h1_in, h2_in;
   wire [255:0] h1_out, h2_out;
   reg [511:0] h1_msg, h2_msg;
   reg h1_start, h2_start;
   wire h1_done, h2_done;
   reg [255:0] tx_data;
   reg tx_start;
   
   wire [255:0] h_init;
	reg [255:0] h_init_reg;
   assign h_init = {32'h6a09e667, 32'hbb67ae85, 32'h3c6ef372, 32'ha54ff53a, 32'h510e527f, 32'h9b05688c, 32'h1f83d9ab, 32'h5be0cd19};

   always@(posedge clk, posedge reset)
      if(reset)
         state <= 1'b0;
      else
         state <= state_next;
         
   always@(*) begin
      state_next = state;
      
      case (state)
         DE_BTC_IDLE:
            if(rx_ready)
               state_next = DE_BTC_INIT;

         DE_BTC_INIT:
            if(h1_done)
               state_next = DE_BTC_BUSY;
               
         DE_BTC_BUSY:
				if(h2_done)
					state_next = DE_BTC_SEND;
					
         DE_BTC_SEND:
            state_next = DE_BTC_IDLE;
      
         default:
            state_next = DE_BTC_IDLE;
      endcase
   end

   always@(posedge clk, posedge reset)
      if(reset) begin
			rx_reg <= 0;
         nonce_reg <= 0;
			h1_in <= 0;
         h1_msg <= 0;
         h1_start <= 1'b0;
			h_init_reg <=0;
		end
      else begin
			h_init_reg <= h_init;
			rx_reg <= ((state == DE_BTC_IDLE) && (rx_ready)) ? rx_net : rx_reg;
         nonce_reg <= ((state == DE_BTC_IDLE) && (rx_ready)) ? rx_net[384 +: 32] : nonce_reg;
         h1_in <= (state == DE_BTC_IDLE) ? h_init_reg : 
						((state == DE_BTC_INIT) && (h1_done)) ? h1_out : h1_in;
         h1_msg <= ((state == DE_BTC_IDLE) && (rx_ready)) ? rx_net[1023:512] :
						((state == DE_BTC_INIT) && (h1_done)) ? rx_reg[511:0] : h1_msg;
         h1_start <= (state == DE_BTC_IDLE) ? rx_ready :
							(state == DE_BTC_INIT) ? h1_done : 1'b0;
      end   

   always@(posedge clk, posedge reset)
      if(reset) begin
         h2_in <= 0;
         h2_msg <= 0;
         h2_start <= 1'b0;
      end
      else begin
         h2_in <= h_init_reg;
         h2_msg <= (h1_done) ? {h1_out, 8'b1000_0000, {29{8'b0}}, 8'b0000_0001, 8'b0} : h2_msg;
         h2_start <= (state == DE_BTC_BUSY) ? h1_done : 1'b0;
      end
		
   de_sha256 sha256_first(.clk(clk), 
                          .reset(reset), 
                          .start(h1_start), 
                          .chunk(h1_msg), 
                          .h_in(h1_in), 
                          .h_out(h1_out), 
                          .done(h1_done));
                          
   de_sha256 sha256_second(.clk(clk), 
                          .reset(reset), 
                          .start(h2_start), 
                          .chunk(h2_msg), 
                          .h_in(h2_in), 
                          .h_out(h2_out), 
                          .done(h2_done));

   always@(posedge clk, posedge reset)
		if(reset) begin
			tx_data <= 0;
			tx_start <= 1'b0;
		end	
		else begin
			tx_data <= ((state == DE_BTC_BUSY) && (h2_done)) ? h2_out : tx_data;
			tx_start <= (state == DE_BTC_BUSY) ? h2_done : 1'b0;
		end
								  
   de_serial #(.CLK(CLK), .BAUD(BAUD_RATE)) serial(
      .clk(clk),
      .reset(reset),
      .tx_hash(tx_data),
      .tx_nonce(nonce_reg),
      .tx_start(tx_start),
      .rx_data(rx_net),
      .rx_data_rdy(rx_ready),
      .rx_line(uart_rx),
      .tx_line(uart_tx)
   );

endmodule
