
module Ma_fn
#(parameter N = 32)
(
   input wire [(N - 1):0] a, b, c,
   output wire [(N - 1):0] res
);

assign res = (a & b) | (a & c) | (b & c);

endmodule

module S0_fn
#(parameter N = 32)
(
   input wire [(N - 1):0] a,
   output wire [(N - 1):0] res
);
   wire [(N - 1):0] a2, a13, a22;

   assign a2 = {a[1:0], a[(N - 1):2]};
   assign a13 = {a[12:0], a[(N - 1):13]};
   assign a22 = {a[21:0], a[(N - 1):22]};

   assign res = a2 ^ a13 ^ a22;
endmodule

module S1_fn
#(parameter N = 32)
(
   input wire [(N - 1):0] a,
   output wire [(N - 1):0] res
);
   wire [(N - 1):0] a6, a11, a25;
   
   assign a6 = {a[5:0], a[(N - 1):6]};
   assign a11 = {a[10:0], a[(N - 1):11]};
   assign a25 = {a[24:0], a[(N - 1):25]};
   
   assign res = a6 ^ a11 ^ a25;
endmodule

module Ch_fn
#(parameter N = 32)
(
   input wire [(N - 1):0] a, b, c,
   output wire [(N - 1):0] res
);

   assign res = (a & b) ^ (~a & c);
endmodule

module s0_fn
#(parameter N = 32)
(
   input wire [(N - 1):0] a,
   output wire [(N - 1):0] res
);
   wire [(N - 1):0] a7, a18, a3;
   
   assign a7 = {a[6:0], a[(N - 1):7]};
   assign a18 = {a[17:0], a[(N - 1):18]};
   assign a3 = a >> 3;

   assign res = a7 ^ a18 ^ a3;
endmodule

module s1_fn
#(parameter N = 32)
(
   input wire [(N - 1):0] a,
   output wire [(N - 1):0] res
);
   wire [(N - 1):0] a17, a19, a10;
   
   assign a17 = {a[16:0], a[(N - 1):17]};
   assign a19 = {a[18:0], a[(N - 1):19]};
   assign a10 = a >> 10;

   assign res = a17 ^ a19 ^ a10;
endmodule
