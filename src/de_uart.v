module de_uart(
   input wire reset,
   input wire clk,
   
   input wire rx_line,
   output wire [7:0] rx_byte,
   output wire rx_rdy,
   
   input wire [7:0] tx_byte,
   input wire tx_en,
   output wire tx_done,
   output wire tx_line
);

   parameter CLK_HZ  = 5000_000; // 5MHZ default clock
   parameter BAUD    = 38400; 
   localparam SRATE  = CLK_HZ / BAUD;
   
   /*************** Sample counter ***************/
   wire cnt_done, cnt_en;
   reg [31:0] cnt_reg, cnt_next;
   // Counter, state register
   always@(posedge clk, posedge reset) begin
      if(reset)
         cnt_reg <= 0;        
      else
         cnt_reg <= cnt_next;
   end
   // Counter, next state logic
   always@(*) begin
      cnt_next = 0;
   
      if(cnt_en) // Default state, counter less than max. value
         if(SRATE > cnt_reg)
            cnt_next = cnt_reg + 1;
         else
            cnt_next = 0;
      else
         cnt_next = SRATE / 2;
   end
   // Counter, output
   assign cnt_done = (cnt_reg == SRATE);

   /*************** RX FSM ***************/
   localparam  RX_IDLE           = 2'b00,
               RX_RECEIVE_START  = 2'b01,
               RX_RECEIVE_BYTE   = 2'b10,
               RX_RECEIVE_DONE   = 2'b11;

   wire bit_rdy, bit_en, byte_rdy;
   reg [1:0] rstate, rstate_next;
   reg [3:0] bcnt, bcnt_next;
   reg [7:0] rdata, rdata_next;

   // Data path
   always@(posedge clk, posedge reset)
   begin
      if(reset) begin
         rdata <= 0;
         bcnt <= 0;
      end
      else begin
         rdata <= rdata_next;
         bcnt <= bcnt_next;
      end
   end
   always@(*) begin
      rdata_next = bit_rdy ? {rx_line, rdata[7:1]} : rdata;
      bcnt_next = !bit_en ? 4'd0 :
                  bit_rdy ? (bcnt + 1'd1) : bcnt;
   end
   assign rx_byte = rdata;
   assign byte_rdy = (bcnt == 4'd8);
   
   // RX, state register
   always@(posedge clk, posedge reset) begin
      if(reset)
         rstate <= RX_IDLE;
      else
         rstate <= rstate_next;
   end
   
   // RX, next state logic
   always@(*) begin
      rstate_next = RX_IDLE;
      
      case (rstate)
         RX_IDLE:
            if(!rx_line)
               rstate_next = RX_RECEIVE_START;
            else
               rstate_next = RX_IDLE;

         RX_RECEIVE_START:
            if(cnt_done)
               rstate_next = RX_RECEIVE_BYTE;
            else
               rstate_next = RX_RECEIVE_START;
                           
         RX_RECEIVE_BYTE:
            if(byte_rdy)
               rstate_next = RX_RECEIVE_DONE;
            else
               rstate_next = RX_RECEIVE_BYTE;
         RX_RECEIVE_DONE:
            if(cnt_done)
               rstate_next = RX_IDLE;
            else
               rstate_next = RX_RECEIVE_DONE;
         default: 
            rstate_next = RX_IDLE;

      endcase
   end
   // Signals
   assign cnt_en = (RX_IDLE != rstate);
   assign bit_en = (RX_RECEIVE_BYTE == rstate);
   assign bit_rdy = cnt_done && bit_en;
   assign rx_rdy = cnt_done && (RX_RECEIVE_DONE == rstate);
   
   /*************** TX Sample Rate Counter ***************/
   wire tx_cnt_en, tx_cnt_done;
   reg [31:0] tx_cnt, tx_cnt_next;
   always@(posedge clk, posedge reset) begin
      if(reset)
         tx_cnt <= 0;
      else
         tx_cnt <= tx_cnt_next;
   end
   
   always@(*) begin
      tx_cnt_next = 0;
      
      if(tx_cnt_en)
         if(SRATE > tx_cnt)
            tx_cnt_next = tx_cnt + 1;
         else
            tx_cnt_next = 0;
      else
         tx_cnt_next = 0;
   end
   assign tx_cnt_done = (SRATE == tx_cnt);

   /*************** TX FSM ***************/
   localparam  TX_IDLE        = 2'b00,
               TX_XSMIT_START = 2'b01,
               TX_XSMIT_BYTE  = 2'b10,
               TX_XSMIT_END   = 2'b11;
   reg [1:0] tstate, tstate_next;
   reg [7:0] tx_bcnt, tx_bcnt_next;
   wire tx_start, tx_bit_en, tx_bit_done, tx_byte_done;
   
   always@(posedge clk, posedge reset) begin
      if(reset)
         tstate <= TX_IDLE;
      else
         tstate <= tstate_next;
   end
   
   always@(*) begin
      tstate_next = TX_IDLE;
      
      case (tstate)
         TX_IDLE:
            if(tx_en)
               tstate_next = TX_XSMIT_START;
            else
               tstate_next = TX_IDLE;
         TX_XSMIT_START:
               if(tx_cnt_done)
                  tstate_next = TX_XSMIT_BYTE;
               else
                  tstate_next = TX_XSMIT_START;
         TX_XSMIT_BYTE:
               if(tx_byte_done)
                  tstate_next = TX_XSMIT_END;
               else
                  tstate_next = TX_XSMIT_BYTE;
         TX_XSMIT_END:
               if(tx_cnt_done)
                  tstate_next = TX_IDLE;
               else
                  tstate_next = TX_XSMIT_END;
         default:
            tstate_next = TX_IDLE;
      endcase
   end
   // Enable TX counter
   assign tx_cnt_en     = (TX_IDLE != tstate);     
   // Send start bit
   assign tx_start      = (TX_XSMIT_START == tstate);
   // Enable transmitting data bits
   assign tx_bit_en     = (TX_XSMIT_BYTE == tstate);
   // Data bits are transmitted
   assign tx_bit_done   = tx_bit_en && tx_cnt_done;
   // Transfer completed, trailing bit is transmitted
   assign tx_done       = (TX_XSMIT_END == tstate) && tx_cnt_done;

   /*************** TX Data ***************/
   reg [7:0] tx_data, tx_data_next;
   always@(posedge clk, posedge reset) begin
      if(reset) begin
         tx_data <= 0;
         tx_bcnt <= 0;
      end
      else begin
         tx_data <= tx_data_next;
         tx_bcnt <= tx_bcnt_next;
      end
   end
   always@(*) begin
      tx_data_next = tx_en       ? tx_byte :
                     tx_bit_done ? {1'b0, tx_data[7:1]} : 
                     tx_done     ? 8'b0 : tx_data;
                     
      tx_bcnt_next = !tx_bit_en  ? 8'b0 :
                     tx_bit_done ? (tx_bcnt + 1'b1) : tx_bcnt;
   end
   
   assign tx_line =  tx_start   ? 1'b0 :
                     tx_bit_en && !tx_byte_done  ? tx_data[0] : 1'b1;
   assign tx_byte_done = (tx_bcnt == 4'd8);
      
endmodule
