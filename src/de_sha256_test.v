
`timescale 10ns / 100ps

`include "src/de_sha256.v"

module de_sha256_test;

reg clk, reset, start, done;
reg [511:0] data;
reg [255:0] initial_hash;
wire [255:0] final_hash;


de_sha256 uut(.clk(clk), .reset(reset), .start(start), 
   .chunk(data), .h_in(initial_hash), .h_out(final_hash));

      
always
   #10 clk = ~clk;

initial
begin
   clk = 0;
   reset = 1;
   start = 0;
      
   #10 reset = 0;
   
   @(posedge clk)
   begin
      data = 512'h61626380_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000018;
      initial_hash = 256'h6a09e667_bb67ae85_3c6ef372_a54ff53a_510e527f_9b05688c_1f83d9ab_5be0cd19;
   end
   
   #10 
   
   @(posedge clk)
      start = 1'b1;
   
   @(posedge clk)
      start = 1'b0;
      

         
   #45000
 $display("A \t\tB \t\tC \t\tD \t\tE \t\tF \t\tG \t\tH");
 $display("%x \t%x \t%x \t%x \t%x \t%x \t%x \t%x", 
 uut.gen_rounds[0].h[`A_IDX],
 uut.gen_rounds[0].h[`B_IDX],
 uut.gen_rounds[0].h[`C_IDX],
 uut.gen_rounds[0].h[`D_IDX],
 uut.gen_rounds[0].h[`E_IDX],
 uut.gen_rounds[0].h[`F_IDX],
 uut.gen_rounds[0].h[`G_IDX],
 uut.gen_rounds[0].h[`H_IDX]);     
 
  $display("%x \t%x \t%x \t%x \t%x \t%x \t%x \t%x", 
 uut.gen_rounds[1].h[`A_IDX],
 uut.gen_rounds[1].h[`B_IDX],
 uut.gen_rounds[1].h[`C_IDX],
 uut.gen_rounds[1].h[`D_IDX],
 uut.gen_rounds[1].h[`E_IDX],
 uut.gen_rounds[1].h[`F_IDX],
 uut.gen_rounds[1].h[`G_IDX],
 uut.gen_rounds[1].h[`H_IDX]); 
 
  $display("%x \t%x \t%x \t%x \t%x \t%x \t%x \t%x", 
 uut.gen_rounds[64].h[`A_IDX],
 uut.gen_rounds[64].h[`B_IDX],
 uut.gen_rounds[64].h[`C_IDX],
 uut.gen_rounds[64].h[`D_IDX],
 uut.gen_rounds[64].h[`E_IDX],
 uut.gen_rounds[64].h[`F_IDX],
 uut.gen_rounds[64].h[`G_IDX],
 uut.gen_rounds[64].h[`H_IDX]); 
 
	$display("Final hash:");
   $display("%x \t%x \t%x \t%x \t%x \t%x \t%x \t%x", 
 final_hash[`A_IDX],
 final_hash[`B_IDX],
 final_hash[`C_IDX],
 final_hash[`D_IDX],
 final_hash[`E_IDX],
 final_hash[`F_IDX],
 final_hash[`G_IDX],
 final_hash[`H_IDX]); 
   $stop;
end

endmodule
