`ifndef DE_SHA256
`define DE_SHA256
module de_sha256(
   input wire clk, reset, start,
   input wire [511:0] chunk,
   input wire [255:0] h_in,
   output reg [255:0] h_out,
   output wire done
);

`define IDX(_idx)    (_idx)*32 +: 32

`define A_IDX  `IDX(7)
`define B_IDX  `IDX(6)
`define C_IDX  `IDX(5)
`define D_IDX  `IDX(4)
`define E_IDX  `IDX(3)
`define F_IDX  `IDX(2)
`define G_IDX  `IDX(1)
`define H_IDX  `IDX(0)

   localparam [6:0] ROUNDS = 7'd64;
   localparam [3:0] NHASH = 4'd8;
   localparam [1:0]  IDLE     = 2'b00,
                     BUSY     = 2'b01,
                     DONE     = 2'b10,
                     END      = 2'b11;

   reg done_reg, done_reg_next;

   reg [255:0] h_out_next;

   reg [1:0] state, state_next;
   reg [6:0] cnt, cnt_next;

   function [31:0] k(
      input [5:0] idx
   );
      case (idx)
         6'd0: k = 32'h428a2f98;
         6'd1: k = 32'h71374491;
         6'd2: k = 32'hb5c0fbcf;
         6'd3: k = 32'he9b5dba5;
         6'd4: k = 32'h3956c25b;
         6'd5: k = 32'h59f111f1;
         6'd6: k = 32'h923f82a4;
         6'd7: k = 32'hab1c5ed5;
         6'd8: k = 32'hd807aa98;
         6'd9: k = 32'h12835b01;
         6'd10: k = 32'h243185be;
         6'd11: k = 32'h550c7dc3;
         6'd12: k = 32'h72be5d74;
         6'd13: k = 32'h80deb1fe;
         6'd14: k = 32'h9bdc06a7;
         6'd15: k = 32'hc19bf174;
         6'd16: k = 32'he49b69c1;
         6'd17: k = 32'hefbe4786;
         6'd18: k = 32'h0fc19dc6;
         6'd19: k = 32'h240ca1cc;
         6'd20: k = 32'h2de92c6f;
         6'd21: k = 32'h4a7484aa;
         6'd22: k = 32'h5cb0a9dc;
         6'd23: k = 32'h76f988da;
         6'd24: k = 32'h983e5152;
         6'd25: k = 32'ha831c66d;
         6'd26: k = 32'hb00327c8;
         6'd27: k = 32'hbf597fc7;
         6'd28: k = 32'hc6e00bf3;
         6'd29: k = 32'hd5a79147;
         6'd30: k = 32'h06ca6351;
         6'd31: k = 32'h14292967;
         6'd32: k = 32'h27b70a85;
         6'd33: k = 32'h2e1b2138;
         6'd34: k = 32'h4d2c6dfc;
         6'd35: k = 32'h53380d13;
         6'd36: k = 32'h650a7354;
         6'd37: k = 32'h766a0abb;
         6'd38: k = 32'h81c2c92e;
         6'd39: k = 32'h92722c85;
         6'd40: k = 32'ha2bfe8a1;
         6'd41: k = 32'ha81a664b;
         6'd42: k = 32'hc24b8b70;
         6'd43: k = 32'hc76c51a3;
         6'd44: k = 32'hd192e819;
         6'd45: k = 32'hd6990624;
         6'd46: k = 32'hf40e3585;
         6'd47: k = 32'h106aa070;
         6'd48: k = 32'h19a4c116;
         6'd49: k = 32'h1e376c08;
         6'd50: k = 32'h2748774c;
         6'd51: k = 32'h34b0bcb5;
         6'd52: k = 32'h391c0cb3;
         6'd53: k = 32'h4ed8aa4a;
         6'd54: k = 32'h5b9cca4f;
         6'd55: k = 32'h682e6ff3;
         6'd56: k = 32'h748f82ee;
         6'd57: k = 32'h78a5636f;
         6'd58: k = 32'h84c87814;
         6'd59: k = 32'h8cc70208;
         6'd60: k = 32'h90befffa;
         6'd61: k = 32'ha4506ceb;
         6'd62: k = 32'hbef9a3f7;
         6'd63: k = 32'hc67178f2;
      endcase
   
   endfunction
   
   generate
      genvar i;
      for(i = 0; i < ROUNDS; i = i + 1) begin: gen_sch_array
			reg [31:0] w;
			
         if(i < 16)
            always@(posedge clk, posedge reset)
               if(reset)
                  w <= 0;
               else
                  w <= ((IDLE == state) && (start)) ? chunk[`IDX(15 - i)] : w;
         else begin
				wire [31:0] s0, s1;

            s0_fn s0_mod(gen_sch_array[i - 15].w, s0);
            s1_fn s1_mod(gen_sch_array[i - 2].w, s1);
         
            always@(posedge clk, posedge reset)
               if(reset)
                  w <= 0;
               else
                  w <= gen_sch_array[i - 16].w + s0 + gen_sch_array[i - 7].w + s1;
         end
      end // gen_sch_array
   endgenerate
   
   generate
      genvar j;    

      for(j = 0; j < (ROUNDS + 1); j = j + 1) begin: gen_rounds
			reg [(32*NHASH - 1):0] h, h_next;
         
         if(0 == j)
            always@(posedge clk, posedge reset)
               if(reset)
                  h <= 0;
               else
                  h <= ((IDLE == state) && (start)) ? h_in : h;
         else begin
            wire [31:0] S1, ch, S0, maj, temp1, temp2;
            
            S1_fn S1_mod(gen_rounds[j - 1].h[`E_IDX], S1);
            Ch_fn ch_mod(gen_rounds[j - 1].h[`E_IDX], gen_rounds[j - 1].h[`F_IDX], gen_rounds[j - 1].h[`G_IDX], ch);

            S0_fn S0_mod(gen_rounds[j - 1].h[`A_IDX], S0);
            Ma_fn Ma_mod(gen_rounds[j - 1].h[`A_IDX], gen_rounds[j - 1].h[`B_IDX], gen_rounds[j - 1].h[`C_IDX], maj);
            
            assign temp1 = gen_rounds[j - 1].h[`H_IDX] + S1 + ch + k(j - 1) + gen_sch_array[j - 1].w;
            assign temp2 = S0 + maj;

            always@(posedge clk, posedge reset) begin
               if(reset)
                  h <= 0;
               else
                  h <= h_next;
            end
				always@(*) begin
					h_next[`H_IDX] = gen_rounds[j - 1].h[`G_IDX];
					h_next[`G_IDX] = gen_rounds[j - 1].h[`F_IDX];
					h_next[`F_IDX] = gen_rounds[j - 1].h[`E_IDX];
					h_next[`E_IDX] = gen_rounds[j - 1].h[`D_IDX] + temp1;
					h_next[`D_IDX] = gen_rounds[j - 1].h[`C_IDX];
					h_next[`C_IDX] = gen_rounds[j - 1].h[`B_IDX];
					h_next[`B_IDX] = gen_rounds[j - 1].h[`A_IDX];
					h_next[`A_IDX] = temp1 + temp2;
				end
         end
      end // gen_rounds
   endgenerate
   
   always@(posedge clk, posedge reset) begin
      if(reset)
         h_out <= 0;
      else begin
         h_out[`A_IDX] <= gen_rounds[0].h[`A_IDX] + gen_rounds[ROUNDS].h[`A_IDX];
         h_out[`B_IDX] <= gen_rounds[0].h[`B_IDX] + gen_rounds[ROUNDS].h[`B_IDX];
         h_out[`C_IDX] <= gen_rounds[0].h[`C_IDX] + gen_rounds[ROUNDS].h[`C_IDX];
         h_out[`D_IDX] <= gen_rounds[0].h[`D_IDX] + gen_rounds[ROUNDS].h[`D_IDX];
         h_out[`E_IDX] <= gen_rounds[0].h[`E_IDX] + gen_rounds[ROUNDS].h[`E_IDX];
         h_out[`F_IDX] <= gen_rounds[0].h[`F_IDX] + gen_rounds[ROUNDS].h[`F_IDX];
         h_out[`G_IDX] <= gen_rounds[0].h[`G_IDX] + gen_rounds[ROUNDS].h[`G_IDX];
         h_out[`H_IDX] <= gen_rounds[0].h[`H_IDX] + gen_rounds[ROUNDS].h[`H_IDX];
      end  
   end

   always@(posedge clk, posedge reset)
      if(reset) begin
         state <= IDLE;
         cnt <= 7'b0;
         done_reg <= 1'b0;
      end
      else begin
         state <= state_next;
         cnt <= cnt_next;
         done_reg <= done_reg_next;
      end
   
   always@(*) begin
      state_next = state;
      cnt_next = cnt;
      done_reg_next = done_reg;

		case (state)
         IDLE: begin
            done_reg_next = 1'b0;
            cnt_next = 7'b0;
            
            if(start)
               state_next = BUSY;
         end
         BUSY:
            if(cnt >= ROUNDS)
               state_next = DONE;
            else
               cnt_next = cnt + 1'b1;

         DONE: begin
            state_next = END;
            done_reg_next = 1'b1;
         end
         END: begin
            state_next = IDLE;
				done_reg_next = 1'b0;
			end	
      default:
         state_next = IDLE;
         
      endcase
   end
   
   assign done = done_reg;
      
endmodule
`endif