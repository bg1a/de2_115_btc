Bitcoin Calculation for DE2-115
===============================

Overview
--------
Quartus project to calculate a bitcoin on DE2-115 Terasic board.


Check SHA-256
-------------
We can use Linux sha256sum or openssl tools:

echo -en 'xxx' | openssl dgst -sha256 -binary | openssl dgst -sha256


Links
-----
- habrahabr.ru/post/258181/
- en.wikipedia.org/wiki/SHA-2
- csrc.nist.gov/publications/fips/fips180-2/fips180-2.pdf
